FROM php:fpm
COPY --from=composer:latest /usr/bin/composer /usr/local/bin/composer
WORKDIR /code
RUN apt-get update && apt-get install -y git-core libzip-dev zip && docker-php-ext-install zip
COPY composer.json /code/

RUN composer install --no-interaction
COPY . .
RUN composer dump-autoload

EXPOSE 9000
